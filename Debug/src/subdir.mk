################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Image.cpp \
../src/Main.cpp \
../src/matrix_ops.cpp \
../src/svd.cpp \
../src/utils.cpp 

OBJS += \
./src/Image.o \
./src/Main.o \
./src/matrix_ops.o \
./src/svd.o \
./src/utils.o 

CPP_DEPS += \
./src/Image.d \
./src/Main.d \
./src/matrix_ops.d \
./src/svd.d \
./src/utils.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/System/Library/Frameworks/OpenCL.framework/Versions/A/Headers -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


