#ifndef MACROS_H_
#define MACROS_H_

#if _WIN32
#include <CL\cl.h>
#elif __APPLE__
#include <opencl.h>
#else
#include <oclUtils.h>
#endif

#define NO_OF_IMGS			381
#define START_IMG_NO		290
#define END_IMG_NO			322
#define IMG_ROW 			384
#define IMG_COL 			512
#define M_X_N				(IMG_ROW * IMG_COL)
#define STEP 				2
#define ROW 				M_X_N
#define COL					((END_IMG_NO - START_IMG_NO) / STEP)
#define NUM_OF_KERNELS		11
#define TILE_SIZE_MNP		8
#define BLOCK_DIM			16
#define MAT_VEC_GRP_SIZE	16

#if _WIN32
#define PLATFORM_NAME			"Intel"
#define SVD_KERNEL_FILE_PATH	"svd.cl"
#elif __APPLE__
#define PLATFORM_NAME			"Intel"
#define SVD_KERNEL_FILE_PATH	"src/svd.cl"
#else
#define PLATFORM_NAME			"NVIDIA CUDA"
#define SVD_KERNEL_FILE_PATH	"svd.cl"
#endif

#define HEADER_LENGTH 	1078 // This is for BMP
#define MAX_ITERATIONS	4000

#define IDX(i, j, n)	(((i) * n) + (j))
#define MAX_R(X, Y)		(((X) > (Y)) ? (X) : (Y))
#define FILE_TYPE		".bmp"

#if 0
#define USE_POWER_SVD
#else
#define USE_SVD_OPENCL
#endif

#if _WIN32

#define SRC_PATH		"C:\\Users\\Ankit\\Documents\\Visual Studio 2013\\Projects\\BackgroundModelling\\Synced\\src\\dataset\\img_"
#define LOWRANK_PATH	"C:\\Users\\Ankit\\Documents\\Visual Studio 2013\\Projects\\BackgroundModelling\\Synced\\src\\dataset_lowrank\\lowrank_"
#define SPARSE_PATH		"C:\\Users\\Ankit\\Documents\\Visual Studio 2013\\Projects\\BackgroundModelling\\Synced\\src\\dataset_sparse\\sparse_"
typedef cl_float rpca_datatype;

#elif __APPLE__

#define SRC_PATH		"src/dataset/img_"
#define LOWRANK_PATH	"src/dataset_lowrank/lowrank_"
#define SPARSE_PATH		"src/dataset_sparse/sparse_"
typedef cl_float rpca_datatype;

#else

#define SRC_PATH		"dataset/img_"
#define LOWRANK_PATH	"dataset_lowrank/lowrank_"
#define SPARSE_PATH		"dataset_sparse/sparse_"
typedef cl_float rpca_datatype;

#endif //_WIND32
#endif //MACROS_H_
