/*
* svd.cpp
*
*  Created on: Dec 11, 2014
*      Author: saikarthikreddyginni
*      Author: ankitgupta
*/

#include <iostream>
#include <iomanip>
#include "svd.h"
#include "utils.h"
#include "matrix_ops.h"
#include <cmath>

using namespace std;

#define SIGN(a, b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

static rpca_datatype dmaxarg1, dmaxarg2;
#define DMAX(a, b) (dmaxarg1 = (a), dmaxarg2 = (b), (dmaxarg1) > (dmaxarg2) ? \
	(dmaxarg1) : (dmaxarg2))

static cl_uint iminarg1, iminarg2;
#define IMIN(a, b) (iminarg1 = (a), iminarg2 = (b), (iminarg1) < (iminarg2) ? \
	(iminarg1) : (iminarg2))

rpca_datatype *Sigma_matrix;
rpca_datatype *Ys;
rpca_datatype *V_vec;
rpca_datatype *V_vec1;
rpca_datatype *U_vec;
rpca_datatype *U_matrix;

rpca_datatype *alloc_vector(cl_uint n)
{
	rpca_datatype *v;
	v = (rpca_datatype *)malloc(n * sizeof(rpca_datatype));
	return v;
}

rpca_datatype pythag(rpca_datatype a, rpca_datatype b)
/* compute (a2 + b2)^1/2 without destructive underflow or overflow */
{
	rpca_datatype absa, absb;
	absa = fabs(a);
	absb = fabs(b);
	if (absa > absb)
	{
		return rpca_datatype(absa * sqrt(1.0 + (absb / absa) * (absb / absa)));
	}
	else
	{
		return rpca_datatype(absb == 0.0 ? 0.0 : absb * sqrt(1.0 + (absa / absb) * (absa / absb)));
	}
}

void svdcmp_linear(rpca_datatype *aat_eig_vec, cl_uint m, cl_uint n, rpca_datatype *sigma, rpca_datatype *ata_eig_vec)
{
	cl_int flag;
	cl_int i;
	cl_int its;
	cl_int j;
	cl_int jj;
	cl_int k;
	cl_int l;
	cl_int nm;

	rpca_datatype anorm;
	rpca_datatype c;
	rpca_datatype f;
	rpca_datatype g;
	rpca_datatype h;
	rpca_datatype s;
	rpca_datatype scale;
	rpca_datatype x;
	rpca_datatype y;
	rpca_datatype z;
	rpca_datatype *rv1;

	rv1 = alloc_vector(n);
	g = 0.0;
	scale = 0.0;
	anorm = 0.0;

	/* Householder reduction to bi-diagonal form */
	for (i = 0; i < (cl_int)n; i++)
	{
		l = i + 1;
		rv1[i] = scale * g;
		g = 0.0;
		s = 0.0;
		scale = 0.0;

		if (i < (cl_int)m)
		{
			for (k = i; k < (cl_int)m; k++)
			{
				scale = scale + fabs(aat_eig_vec[IDX(k, i, n)]);
			}

			if (scale)
			{
				for (k = i; k < (cl_int)m; k++)
				{
					aat_eig_vec[IDX(k, i, n)] = aat_eig_vec[IDX(k, i, n)] / scale;
					s = s + (aat_eig_vec[IDX(k, i, n)] * aat_eig_vec[IDX(k, i, n)]);
				}

				f = aat_eig_vec[IDX(i, i, n)];
				g = -SIGN(sqrt(s), f);
				h = (f * g) - s;
				aat_eig_vec[IDX(i, i, n)] = f - g;

				for (j = l; j < (cl_int)n; j++)
				{
					for (s = 0.0, k = i; k < (cl_int)m; k++)
					{
						s += aat_eig_vec[IDX(k, i, n)] * aat_eig_vec[IDX(k, j, n)];
					}

					f = s / h;
					for (k = i; k < (cl_int)m; k++)
					{
						aat_eig_vec[IDX(k, j, n)] += (f * aat_eig_vec[IDX(k, i, n)]);
					}
				}

				for (k = i; k < (cl_int)m; k++)
				{
					aat_eig_vec[IDX(k, i, n)] *= scale;
				}
			}
		}

		sigma[i] = scale * g;
		g = 0.0;
		s = 0.0;
		scale = 0.0;

		if ((i < (cl_int)m) && (i != (n - 1)))
		{
			for (k = l; k < (cl_int)n; k++)
			{
				scale += fabs(aat_eig_vec[IDX(i, k, n)]);
			}

			if (scale)
			{
				for (k = l; k < (cl_int)n; k++)
				{
					aat_eig_vec[IDX(i, k, n)] /= scale;
					s += aat_eig_vec[IDX(i, k, n)] * aat_eig_vec[IDX(i, k, n)];
				}

				f = aat_eig_vec[IDX(i, l, n)];
				g = -SIGN(sqrt(s), f);
				h = f * g - s;
				aat_eig_vec[IDX(i, l, n)] = f - g;

				for (k = l; k < (cl_int)n; k++)
				{
					rv1[k] = aat_eig_vec[IDX(i, k, n)] / h;
				}

				for (j = l; j < (cl_int)m; j++)
				{
					for (s = 0.0, k = l; k < (cl_int)n; k++)
					{
						s += aat_eig_vec[IDX(j, k, n)] * aat_eig_vec[IDX(i, k, n)];
					}

					for (k = l; k < (cl_int)n; k++)
					{
						aat_eig_vec[IDX(j, k, n)] += s * rv1[k];
					}
				}

				for (k = l; k < (cl_int)n; k++)
				{
					aat_eig_vec[IDX(i, k, n)] *= scale;
				}
			}
		}

		anorm = DMAX(anorm, (fabs(sigma[i]) + fabs(rv1[i])));
	}

	/* Accumulation of right-hand transformations. */
	for (i = (n - 1); i >= 0; i--)
	{
		if (i < (cl_int)(n - 1))
		{
			if (g)
			{
				/* Double division to avoid possible underflow. */
				for (j = l; j < (cl_int)n; j++)
				{
					ata_eig_vec[IDX(j, i, n)] = (aat_eig_vec[IDX(i, j, n)] / aat_eig_vec[IDX(i, l, n)]) / g;
				}

				for (j = l; j < (cl_int)n; j++)
				{
					for (s = 0.0, k = l; k < (cl_int)n; k++)
					{
						s += aat_eig_vec[IDX(i, k, n)] * ata_eig_vec[IDX(k, j, n)];
					}

					for (k = l; k < (cl_int)n; k++)
					{
						ata_eig_vec[IDX(k, j, n)] += s * ata_eig_vec[IDX(k, i, n)];
					}
				}
			}

			for (j = l; j < (cl_int)n; j++)
			{
				ata_eig_vec[IDX(i, j, n)] = 0.0;
				ata_eig_vec[IDX(j, i, n)] = 0.0;
			}
		}

		ata_eig_vec[IDX(i, i, n)] = 1.0;
		g = rv1[i];
		l = i;
	}

	/* Accumulation of left-hand transformations. */
	for (i = (cl_int)(IMIN(m, n) - 1); i >= 0; i--)
	{
		l = i + 1;
		g = sigma[i];

		for (j = l; j < (cl_int)n; j++)
		{
			aat_eig_vec[IDX(i, j, n)] = 0.0;
		}

		if (g)
		{
			g = rpca_datatype(1.0 / g);
			for (j = l; j < (cl_int)n; j++)
			{
				for (s = 0.0, k = l; k < (cl_int)m; k++)
				{
					s += aat_eig_vec[IDX(k, i, n)] * aat_eig_vec[IDX(k, j, n)];
				}

				f = (s / aat_eig_vec[IDX(i, i, n)]) * g;
				for (k = i; k < (cl_int)m; k++)
				{
					aat_eig_vec[IDX(k, j, n)] += f * aat_eig_vec[IDX(k, i, n)];
				}
			}

			for (j = i; j < (cl_int)m; j++)
			{
				aat_eig_vec[IDX(j, i, n)] *= g;
			}
		}
		else
		{
			for (j = i; j < (cl_int)m; j++)
			{
				aat_eig_vec[IDX(j, i, n)] = 0.0;
			}
		}

		++aat_eig_vec[IDX(i, i, n)];
	}

	/* Diagonalization of the bi-diagonal form. */
	for (k = n - 1; k >= 0; k--)
	{
		for (its = 1; its <= 30; its++)
		{
			flag = 1;
			/* Test for splitting. */
			for (l = k; l >= 0; l--)
			{
				/* Note that rv1[1] is always zero. */
				nm = l - 1;
				if ((rpca_datatype(fabs(rv1[l]) + anorm)) == anorm)
				{
					flag = 0;
					break;
				}

				if (nm > -1)
				{
					if ((rpca_datatype(fabs(sigma[nm]) + anorm)) == anorm)
					{
						break;
					}
				}
			}

			if (flag)
			{
				/* Cancellation of rv1[l], if l > 1. */
				c = 0.0;
				s = 1.0;

				for (i = l; i <= k; i++)
				{
					f = s * rv1[i];
					rv1[i] = c * rv1[i];
					if ((rpca_datatype)(fabs(f) + anorm) == anorm)
					{
						break;
					}

					g = sigma[i];
					h = pythag(f, g);
					sigma[i] = h;
					h = rpca_datatype(1.0 / h);
					c = g * h;
					s = -(f * h);

					for (j = 0; j < cl_int(m); j++)
					{
						y = aat_eig_vec[IDX(j, nm, n)];
						z = aat_eig_vec[IDX(j, i, n)];
						aat_eig_vec[IDX(j, nm, n)] = (y * c) + (z * s);
						aat_eig_vec[IDX(j, i, n)] = (z * c) - (y * s);
					}
				}
			}

			z = sigma[k];
			/* Convergence. */
			if (l == k)
			{
				/* Singular value is made nonnegative. */
				if (z < 0.0)
				{
					sigma[k] = -z;
					for (j = 0; j < cl_int(n); j++)
					{
						ata_eig_vec[IDX(j, k, n)] = -ata_eig_vec[IDX(j, k, n)];
					}
				}
				break;
			}

			if (its == 30)
			{
				printf("no convergence in 30 svdcmp iterations\n");
			}

			/* Shift from bottom 2-by-2 minor. */
			x = sigma[l];
			nm = k - 1;
			y = sigma[nm];
			g = rv1[nm];
			h = rv1[k];
			f = rpca_datatype(((y - z) * (y + z) + (g - h) * (g + h)) / (2.0 * h *y));
			g = pythag(f, 1.0);
			f = ((x - z) * (x + z) + h * ((y / (f + SIGN(g, f))) - h)) / x;

			/* Next QR transformation: */
			c = 1.0;
			s = 1.0;

			for (j = l; j <= nm; j++)
			{
				i = j + 1;
				g = rv1[i];
				y = sigma[i];
				h = s * g;
				g = c * g;
				z = pythag(f, h);
				rv1[j] = z;
				c = f / z;
				s = h / z;
				f = x * c + g * s;
				g = g * c - x * s;
				h = y * s;
				y *= c;

				for (jj = 0; jj < cl_int(n); jj++)
				{
					x = ata_eig_vec[IDX(jj, j, n)];
					z = ata_eig_vec[IDX(jj, i, n)];
					ata_eig_vec[IDX(jj, j, n)] = x * c + z * s;
					ata_eig_vec[IDX(jj, i, n)] = z * c - x * s;
				}

				z = pythag(f, h);
				/* Rotation can be arbitrary if z = 0. */
				sigma[j] = z;

				if (z)
				{
					z = rpca_datatype(1.0 / z);
					c = f * z;
					s = h * z;
				}

				f = c * g + s * y;
				x = c * y - s * g;

				for (jj = 0; jj < cl_int(m); jj++)
				{
					y = aat_eig_vec[IDX(jj, j, n)];
					z = aat_eig_vec[IDX(jj, i, n)];
					aat_eig_vec[IDX(jj, j, n)] = y * c + z * s;
					aat_eig_vec[IDX(jj, i, n)] = z * c - y * s;
				}
			}

			rv1[l] = 0.0;
			rv1[k] = f;
			sigma[k] = x;
		}
	}

	free(rv1);
}

void svdcmp_pow(rpca_datatype *aat_eig_vec, cl_uint m, cl_uint n, rpca_datatype *sigma,
	rpca_datatype *ata_eig_vec, rpca_datatype mu) {
	rpca_datatype *v;
	rpca_datatype *u;
	rpca_datatype *temp;
	rpca_datatype *temp1;
	rpca_datatype *v_n;
	rpca_datatype sigma_t;
	rpca_datatype delta_svd = rpca_datatype(pow(10, -5) * sqrt(n));

	v = (rpca_datatype *)malloc(sizeof(rpca_datatype) * n);
	v_n = (rpca_datatype *)malloc(sizeof(rpca_datatype) * n);
	u = (rpca_datatype *)malloc(sizeof(rpca_datatype) * m);
	temp = (rpca_datatype *)malloc(sizeof(rpca_datatype) * m * n);
	temp1 = (rpca_datatype *)malloc(sizeof(rpca_datatype) * m * n);

	init_mat_zero(temp1, m * n);
	init_mat_zero(sigma, n);

	for (cl_uint i = 0; i < n; i++) {
		for (cl_uint j = 0; j < n; j++) {
			srand(cl_uint(time(NULL)) + i * j + 8897);
			v[j] = (rpca_datatype)rand();
		}

		scalar_mul_matrix(v, v, 1 / frob_norm(v, n), n);
		matrix_transpose1(aat_eig_vec, temp, m, n);

		while (true) {
			matrix_multiplication(aat_eig_vec, v, u, m, n, 1);
			scalar_mul_matrix(u, u, 1 / frob_norm(u, m), m);
			matrix_multiplication(temp, u, v_n, n, m, 1);
			sigma_t = frob_norm(v_n, n);
			scalar_mul_matrix(v_n, v_n, 1 / sigma_t, n);
			subtract_matrix(v_n, v, v, n);
			if (frob_norm(v, n) < delta_svd) {
				if (sigma_t < 1 / mu) {
					break;
				}
				sigma[i] = sigma_t;
				for (cl_uint k = 0; k < m; k++) {
					temp1[IDX(k, i, n)] = u[k];
				}

				for (cl_uint k = 0; k < n; k++) {
					ata_eig_vec[IDX(k, i, n)] = v_n[k];
				}

				break;
			}
			else {
				for (cl_uint k = 0; k < n; k++) {
					v[k] = v_n[k];
				}
			}
		}
		if (sigma_t < 1 / mu) {
			break;
		}

		matrix_transpose1(v_n, v, n, 1);
		matrix_multiplication(u, v, temp, m, 1, n);
		scalar_mul_matrix(temp, temp, sigma_t, m * n);
		subtract_matrix(aat_eig_vec, temp, aat_eig_vec, m * n);
	}

	for (cl_uint k = 0; k < m * n; k++) {
		aat_eig_vec[k] = temp1[k];
	}

	free(v);
	free(v_n);
	free(u);
	free(temp);
	free(temp1);
}

void svd_thresholding(rpca_datatype * M, rpca_datatype *S, rpca_datatype *Y, rpca_datatype *L, rpca_datatype mu) {
	rpca_datatype *sigma = (rpca_datatype *)malloc(sizeof(rpca_datatype) * COL);
	rpca_datatype *v = (rpca_datatype *)malloc(sizeof(rpca_datatype) * COL * COL);
	rpca_datatype *diag = (rpca_datatype *)malloc(sizeof(rpca_datatype) * COL * COL);
	rpca_datatype *temp = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW * COL);
	rpca_datatype *Ys = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW * COL);

	subtract_matrix(M, S, L, ROW * COL);
	scalar_mul_matrix(Y, Ys, (1 / mu), ROW * COL);
	add_matrix(L, Ys, L, ROW * COL);

#if defined USE_POWER_SVD
	svdcmp_pow(L, ROW, COL, sigma, v, mu);
#else
	svdcmp_linear(L, ROW, COL, sigma, v);
#endif

	disp_vector_lin(sigma, COL);
	matrix_transpose(v, COL);

	init_mat_zero(diag, COL * COL);
	init_mat_zero(temp, ROW * COL);

	for (cl_uint i = 0; i < COL; i++) {
		diag[IDX(i, i, COL)] = shrinkage(sigma[i], (1 / mu));
	}

	matrix_multiplication(L, diag, temp, ROW, COL, COL);
	matrix_multiplication(temp, v, L, ROW, COL, COL);

	free(sigma);
	free(v);
	free(diag);
	free(temp);
	free(Ys);
}

void init_svd_pow_thresholding_opencl() {
	Sigma_matrix = (rpca_datatype *)malloc(sizeof(rpca_datatype) * COL * COL);
	Ys = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW * COL);
	V_vec = (rpca_datatype *)malloc(sizeof(rpca_datatype) * COL);
	V_vec1 = (rpca_datatype *)malloc(sizeof(rpca_datatype) * COL);
	U_vec = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW);
	U_matrix = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW * COL);
}

void svd_pow_thresholding_opencl(rpca_cl_params *cl_params, rpca_datatype * M, rpca_datatype *S, rpca_datatype *Y, rpca_datatype *L, rpca_datatype mu) {
	rpca_datatype sigma_t;
	rpca_datatype delta_svd = rpca_datatype(pow(10, -5) * sqrt(ROW));
	rpca_datatype frob_norm_gpu;
	cl_int clStatus;

	subtract_matrix(M, S, L, ROW * COL);
	scalar_mul_matrix(Y, Ys, (1 / mu), ROW * COL);
	add_matrix(L, Ys, L, ROW * COL);

	init_mat_zero(U_matrix, ROW * COL);
	init_mat_zero(U_vec, ROW);
	init_mat_zero(Sigma_matrix, COL * COL);
	init_mat_rand(V_vec, COL);
	init_mat_rand(V_vec1, COL);

	clStatus = clEnqueueWriteBuffer(cl_params->command_queue,
		cl_params->Sigma_matrix_buf, CL_TRUE, 0, COL * COL * sizeof(rpca_datatype), (const void *)Sigma_matrix, 0, NULL, NULL);
	clStatus = clEnqueueWriteBuffer(cl_params->command_queue,
		cl_params->V_vec_buf, CL_TRUE, 0, COL * sizeof(rpca_datatype), (const void *)V_vec, 0, NULL, NULL);
	clStatus = clEnqueueWriteBuffer(cl_params->command_queue,
		cl_params->V_vec1_buf, CL_TRUE, 0, COL * sizeof(rpca_datatype), (const void *)V_vec1, 0, NULL, NULL);
	clStatus = clEnqueueWriteBuffer(cl_params->command_queue,
		cl_params->U_vec_buf, CL_TRUE, 0, ROW * sizeof(rpca_datatype), (const void *)U_vec, 0, NULL, NULL);
	clStatus = clEnqueueWriteBuffer(cl_params->command_queue,
		cl_params->U_matrix_buf, CL_TRUE, 0, ROW * COL * sizeof(rpca_datatype), (const void *)U_matrix, 0, NULL, NULL);
	clStatus = clEnqueueWriteBuffer(cl_params->command_queue,
		cl_params->L_buf, CL_TRUE, 0, ROW * COL * sizeof(rpca_datatype), (const void *)L, 0, NULL, NULL);

	for (cl_uint i = 0; i < COL; i++) {
		if (i == 1) {
			exec_mat_mat_copy_kernel(cl_params, cl_params->V_vec1_buf, cl_params->V_vec_buf, COL);
		}
		else if (i > 1) {
			/*for (cl_uint j = 0; j < COL; j++) {
				V_vec[j] = V_matrix[IDX(j, i - 1, COL)] - V_matrix[IDX(j, i - 2, COL)];
			}*/
			exec_mat_vec_spcl_op_kernel(cl_params, cl_params->V_matrix_buf, cl_params->V_vec_buf, ROW, COL, i);
		}

		//frob_norm(V_vec, COL)
		exec_frob_norm_kernel(cl_params, cl_params->V_vec_buf, COL, frob_norm_gpu);

		//scalar_mul_matrix(V_vec, V_vec, 1 / frob_norm(V_vec, COL), COL);
		exec_scalar_mat_mult_kernel(cl_params, cl_params->V_vec_buf, cl_params->V_vec_buf, COL, 1 / frob_norm_gpu);

		//matrix_transpose1(L, AT_matrix, ROW, COL);
		exec_mat_transpose_kernel(cl_params, cl_params->L_buf, cl_params->AT_matrix_buf, ROW, COL);

		cl_uint iter = 0;
		while (iter < 1000) {

			//matrix_multiplication(L, V_vec, U_vec_k, ROW, COL, 1);
			exec_mat_vec_mult_kernel(cl_params, cl_params->L_buf, cl_params->V_vec_buf, cl_params->U_vec_k_buf, ROW, COL);

			//frob_norm(U_vec_k, ROW)
			exec_frob_norm_kernel(cl_params, cl_params->U_vec_k_buf, ROW, frob_norm_gpu);

			//scalar_mul_matrix(U_vec_k, U_vec_k, 1 / frob_norm(U_vec_k, ROW), ROW);
			exec_scalar_mat_mult_kernel(cl_params, cl_params->U_vec_k_buf, cl_params->U_vec_k_buf, ROW, 1 / frob_norm_gpu);

			//matrix_multiplication(AT_matrix, U_vec_k, V_vec, COL, ROW, 1);
			exec_mat_vec_mult_kernel(cl_params, cl_params->AT_matrix_buf, cl_params->U_vec_k_buf, cl_params->V_vec_buf, COL, ROW);

			//sigma_t = frob_norm(V_vec, COL);
			exec_frob_norm_kernel(cl_params, cl_params->V_vec_buf, COL, sigma_t);

			//scalar_mul_matrix(V_vec, V_vec, 1 / sigma_t, COL);
			exec_scalar_mat_mult_kernel(cl_params, cl_params->V_vec_buf, cl_params->V_vec_buf, COL, 1 / sigma_t);

			//subtract_matrix(U_vec_k, U_vec, U_vec, ROW);
			exec_mat_subt_kernel(cl_params, cl_params->U_vec_k_buf, cl_params->U_vec_buf, cl_params->U_vec_buf, ROW);

			//frob_norm(U_vec, ROW)
			exec_frob_norm_kernel(cl_params, cl_params->U_vec_buf, ROW, frob_norm_gpu);

			//cout << "\n sigma_t inside SVD :" << sigma_t << endl;
			if (frob_norm_gpu < delta_svd) {
				if (sigma_t < 1 / mu) {
					break;
				}

				//Sigma_matrix[IDX(i, i, COL)] = shrinkage(sigma_t, (1 / mu));
				exec_copy_single_val_kernel(cl_params, cl_params->Sigma_matrix_buf, IDX(i, i, COL), shrinkage(sigma_t, (1 / mu)));

				/*for (cl_uint k = 0; k < ROW; k++) {
					U_matrix[IDX(k, i, COL)] = U_vec_k[k];
				}*/
				exec_vec_mat_copy_kernel(cl_params, cl_params->U_vec_k_buf, cl_params->U_matrix_buf, ROW, COL, i);

				/*for (cl_uint k = 0; k < COL; k++) {
					V_matrix[IDX(k, i, COL)] = V_vec[k];
				}*/
				exec_vec_mat_copy_kernel(cl_params, cl_params->V_vec_buf, cl_params->V_matrix_buf, COL, COL, i);

				break;
			}
			else {

				/*for (cl_uint k = 0; k < ROW; k++) {
					U_vec[k] = U_vec_k[k];
				}*/
				exec_mat_mat_copy_kernel(cl_params, cl_params->U_vec_k_buf, cl_params->U_vec_buf, ROW);
			}

			iter++;
		}

		if (sigma_t < 1 / mu) {
			break;
		}

		//matrix_multiplication(U_vec_k, V_vec, AT_matrix, ROW, 1, COL);
		exec_vec_outer_prod_kernel(cl_params, cl_params->U_vec_k_buf, cl_params->V_vec_buf, cl_params->AT_matrix_buf, ROW, COL);

		//scalar_mul_matrix(AT_matrix, AT_matrix, sigma_t, ROW * COL);
		exec_scalar_mat_mult_kernel(cl_params, cl_params->AT_matrix_buf, cl_params->AT_matrix_buf, ROW * COL, sigma_t);

		//subtract_matrix(L, AT_matrix, L, ROW * COL);
		exec_mat_subt_kernel(cl_params, cl_params->L_buf, cl_params->AT_matrix_buf, cl_params->L_buf, ROW * COL);
	}

	/*clStatus = clEnqueueReadBuffer(cl_params->command_queue, cl_params->Sigma_matrix_buf, CL_TRUE, 0, COL * COL * sizeof(rpca_datatype),	Sigma_matrix, 0, NULL, NULL);
	disp_matrix_diag_lin(Sigma_matrix, COL);*/

	//matrix_transpose(V_matrix, COL);
	exec_mat_transpose_kernel(cl_params, cl_params->V_matrix_buf, cl_params->VT_matrix_buf, COL, COL);

	//matrix_multiplication(U_matrix, Sigma_matrix, temp, ROW, COL, COL);
	exec_mat_mult_mnp_kernel(cl_params, cl_params->U_matrix_buf, cl_params->Sigma_matrix_buf, cl_params->temp_buf, ROW, COL, COL);

	//matrix_multiplication(temp, V_matrix, L, ROW, COL, COL);
	exec_mat_mult_mnp_kernel(cl_params, cl_params->temp_buf, cl_params->VT_matrix_buf, cl_params->L_buf, ROW, COL, COL);
	clStatus = clEnqueueReadBuffer(cl_params->command_queue, cl_params->L_buf, CL_TRUE, 0, ROW * COL * sizeof(rpca_datatype), L, 0, NULL, NULL);
}

void deinit_svd_pow_thresholding_opencl() {
	free(Sigma_matrix);
	free(Ys);
	free(V_vec);
	free(V_vec1);
	free(U_vec);
	free(U_matrix);
}

void svd_pow_thresholding_combined(rpca_datatype * M, rpca_datatype *S, rpca_datatype *Y, rpca_datatype *L, rpca_datatype mu) {
	rpca_datatype *V_matrix;
	rpca_datatype *Sigma_matrix;
	rpca_datatype *temp;
	rpca_datatype *Ys;
	rpca_datatype *V_vec;
	rpca_datatype *U_vec;
	rpca_datatype *U_vec_k;
	rpca_datatype *AT_matrix;
	rpca_datatype *U_matrix;

	rpca_datatype sigma_t;
	rpca_datatype delta_svd = rpca_datatype(pow(10, -5) * sqrt(ROW));

	V_matrix = (rpca_datatype *)malloc(sizeof(rpca_datatype) * COL * COL);
	Sigma_matrix = (rpca_datatype *)malloc(sizeof(rpca_datatype) * COL * COL);
	temp = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW * COL);
	Ys = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW * COL);
	V_vec = (rpca_datatype *)malloc(sizeof(rpca_datatype) * COL);
	U_vec = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW);
	U_vec_k = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW);
	AT_matrix = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW * COL);
	U_matrix = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW * COL);

	subtract_matrix(M, S, L, ROW * COL);
	scalar_mul_matrix(Y, Ys, (1 / mu), ROW * COL);
	add_matrix(L, Ys, L, ROW * COL);

	init_mat_zero(U_matrix, ROW * COL);
	init_mat_zero(U_vec, ROW);
	init_mat_zero(Sigma_matrix, COL * COL);

	for (cl_uint i = 0; i < COL; i++) {
		if (i < 2) {
			for (cl_uint j = 0; j < COL; j++) {
				srand(cl_uint(time(NULL)) + i * j + 8897);
				V_vec[j] = (rpca_datatype)rand();
			}
		}
		else
		{
			for (cl_uint j = 0; j < COL; j++) {
				V_vec[j] = V_matrix[IDX(j, i - 1, COL)] - V_matrix[IDX(j, i - 2, COL)];
			}
		}

		scalar_mul_matrix(V_vec, V_vec, 1 / frob_norm(V_vec, COL), COL);
		matrix_transpose1(L, AT_matrix, ROW, COL);

		cl_uint iter = 0;
		while (iter < 1000) {
			matrix_multiplication(L, V_vec, U_vec_k, ROW, COL, 1);
			scalar_mul_matrix(U_vec_k, U_vec_k, 1 / frob_norm(U_vec_k, ROW), ROW);
			matrix_multiplication(AT_matrix, U_vec_k, V_vec, COL, ROW, 1);
			sigma_t = frob_norm(V_vec, COL);
			scalar_mul_matrix(V_vec, V_vec, 1 / sigma_t, COL);
			subtract_matrix(U_vec_k, U_vec, U_vec, ROW);

			if (frob_norm(U_vec, ROW) < delta_svd) {
				if (sigma_t < 1 / mu) {
					break;
				}

				Sigma_matrix[IDX(i, i, COL)] = shrinkage(sigma_t, (1 / mu));
				for (cl_uint k = 0; k < ROW; k++) {
					U_matrix[IDX(k, i, COL)] = U_vec_k[k];
				}

				for (cl_uint k = 0; k < COL; k++) {
					V_matrix[IDX(k, i, COL)] = V_vec[k];
				}

				break;
			}
			else {
				for (cl_uint k = 0; k < ROW; k++) {
					U_vec[k] = U_vec_k[k];
				}
			}
			iter++;
		}

		if (sigma_t < 1 / mu) {
			break;
		}

		matrix_transpose1(V_vec, V_vec, COL, 1);
		matrix_multiplication(U_vec_k, V_vec, AT_matrix, ROW, 1, COL);
		scalar_mul_matrix(AT_matrix, AT_matrix, sigma_t, ROW * COL);
		subtract_matrix(L, AT_matrix, L, ROW * COL);
	}

	//disp_matrix_diag_lin(Sigma_matrix, COL);
	matrix_transpose(V_matrix, COL);
	matrix_multiplication(U_matrix, Sigma_matrix, temp, ROW, COL, COL);
	matrix_multiplication(temp, V_matrix, L, ROW, COL, COL);

	free(V_matrix);
	free(Sigma_matrix);
	free(temp);
	free(Ys);
	free(V_vec);
	free(U_vec);
	free(U_vec_k);
	free(AT_matrix);
	free(U_matrix);
}
