#ifndef MATRIX_OPS_H_
#define MATRIX_OPS_H_
#include "macros.h"
#include "utils.h"

rpca_datatype frob_norm(rpca_datatype *a, cl_uint n);
void shrink_matrix(rpca_datatype *a, cl_uint n, rpca_datatype tau);
void add_matrix(rpca_datatype *a, rpca_datatype *b, rpca_datatype *c, cl_uint n);
void subtract_matrix(rpca_datatype *a, rpca_datatype *b, rpca_datatype *c, cl_uint n);
void scalar_mul_matrix(rpca_datatype *a, rpca_datatype *b, rpca_datatype k, cl_uint n);
void matrix_transpose(rpca_datatype *a, cl_uint n);
void matrix_multiplication(rpca_datatype *a, rpca_datatype *b, rpca_datatype *c, cl_uint m, cl_uint n, cl_uint p);
void matrix_transpose1(rpca_datatype *a, rpca_datatype *b, cl_uint m, cl_uint n);

void exec_mat_mult_mnp_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_mem &c, cl_uint m, cl_uint n, cl_uint p);
void exec_mat_vec_mult_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_mem &c, cl_uint m, cl_uint n);
void exec_mat_vec_mult_kernel1(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_mem &c, cl_uint m, cl_uint n);
void exec_scalar_mat_mult_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_uint n, rpca_datatype scalar);
void exec_vec_outer_prod_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_mem &c, cl_uint m, cl_uint n);
void exec_mat_subt_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_mem &c, cl_uint n);
void exec_mat_transpose_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_uint m, cl_uint n);
void exec_frob_norm_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_uint m, rpca_datatype &result);
void exec_vec_mat_copy_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_uint m, cl_uint n, cl_uint i);
void exec_mat_mat_copy_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_uint n);
void exec_mat_vec_spcl_op_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_uint m, cl_uint n, cl_uint p);
void exec_copy_single_val_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_uint loc, rpca_datatype val);

#endif //MATRIX_OPS_H_