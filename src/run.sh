#!/bin/sh

#gcc -I/usr/local/cuda/include -L/usr/local/cuda/lib64 vector_add.c -o vector_add -lOpenCL

cd dataset_lowrank
rm -rf *.bmp
cd ../dataset_sparse
rm -rf *.bmp
cd ..
rm -f output_log.txt

make -f Makefile

#srun --gres=gpu:1 ./rpcaOpenCL
