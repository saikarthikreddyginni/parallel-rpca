/*
* image.cpp
*
*  Created on: Dec 11, 2014
*      Author: saikarthikreddyginni
*      Author: ankitgupta
*/

#include "image.h"
#include <cmath>

using namespace std;

void imread(char* fileName, unsigned char *imageHeaderData, unsigned char *imageData, cl_uint height, cl_uint width)
{
	ifstream pInFile;
	pInFile.open(fileName, ios::in | ios::binary); // open fileName and read as binary.
	pInFile.seekg(0, ios::beg); //pos filter at beginning of image file.
	pInFile.read(reinterpret_cast<char*>(imageHeaderData), 1078); //read bmp header data into array.
	pInFile.read(reinterpret_cast<char*>(imageData), height * width); //read row into each array entry.
	pInFile.close(); //close stream.
}

void imwrite(char* fileName, unsigned char *imageHeaderData, unsigned char *imageData, cl_uint height, cl_uint width)
{
	ofstream pOutFile;
	pOutFile.open(fileName, ios::out | ios::trunc | ios::binary);
	pOutFile.write(reinterpret_cast<char*>(imageHeaderData), 1078); //write header data onto output
	pOutFile.write(reinterpret_cast<char*>(imageData), height * width); // write new image data.
	pOutFile.close(); //close stream
}

