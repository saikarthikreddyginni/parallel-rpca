#if USE_FLOAT
typedef float rpca_kernel_datatype;
#else
typedef float rpca_kernel_datatype;
#endif

__kernel void mat_mult_mnp_cl(__global rpca_kernel_datatype* a,
						__global rpca_kernel_datatype* b,
						__global rpca_kernel_datatype* c,
						unsigned int m,
						unsigned int n,
						unsigned int l,
						unsigned int tw,
						__local rpca_kernel_datatype *ta,
						__local rpca_kernel_datatype *tb) {
	unsigned int i = get_global_id(0); // Row index of global work item
	unsigned int j = get_global_id(1); // Column index of global work item
	unsigned int u = get_local_id(0); // Row index of local work item
	unsigned int v = get_local_id(1); // Column index of local work item
	unsigned int k = 0; // Used for doing inner product of tiles.
	unsigned int p = 0; // Used for traversing tiled blocks in the matrix
	rpca_kernel_datatype temp = 0.0; // Private memory used for storing intermediate calculations

	for (p = 0; p < (n / tw); p++) {
		/* Load local memory tiles. ta is the tile for matrix A and tb is the
		 * tile for matrix B. Here we need to derive appropriate indices
		 * to load the tiles into local memory from global memory. tw is the
		 * tile width. Also note that, each work item in the work group is
		 * loading only one cells of input matrix. Indexing needs to be done
		 * here such that entire tiles get loaded in parellel. So a T by T width
		 * tile has to load TxT values in local memory, which is performed by
		 * TxT work items in the work group. */
		ta[(u * tw) + v] = a[((((i / tw) * tw) + u) * n) + ((p * tw) + v)];
		tb[(u * tw) + v] = b[(((p * tw) + u) * l) + (((j / tw) * tw) + v)];

		/* Before performing actual calculation, we need to make sure that every
		 * work item has finished its job of loading single values into the
		 * tiles. This can be done by using synchronization barrier. */
		barrier(CLK_LOCAL_MEM_FENCE);

		/* Perform matrix multiplication on the tiles loaded. Here each local
		 * item of the work group is calculating only one cell of output matrix.
		 */
		for (k = 0; k < tw; k++) {
			temp += ta[(u * tw) + k] * tb[(k * tw) + v];
		}

		/* Use synchronization once again just to make sure that every work item
		 * in the work group have done their part of calculation. If barrier had
		 * not been applied here, then some of the work items might have gone
		 * further to the next iteration of the kernel and have started to load
		 * new values in tiles while other threads are not guaranteed to complete
		 * their iteration 1. This has the impact of disturbing the calculations. */
		barrier(CLK_LOCAL_MEM_FENCE);
	}

	/* Store the value in the output matrix cell.*/
	c[(i * l) + j] = temp;
}

__kernel void mat_vec_mult_cl(const __global rpca_kernel_datatype* M,
						const __global rpca_kernel_datatype* V,
						__global rpca_kernel_datatype* W,
						uint m,
						uint n)
{
	// Each work-item handles as many matrix rows as necessary
	for (uint i = get_global_id(0); i < m; i += get_global_size(0)) {
		// Row pointer
		const __global rpca_kernel_datatype* row = M + i * n;

		// Compute dot product
		rpca_kernel_datatype dotProduct = 0;

		for (uint j = 0; j < n; j++) {
			dotProduct += row[j] * V[j];
		}

		// Write result to global memory
		W[i] = dotProduct;
	}
}

__kernel void scalar_mat_mult_cl(__global rpca_kernel_datatype *M,
									__global rpca_kernel_datatype *W,
									rpca_kernel_datatype k) {
	unsigned int i = get_global_id(0);
	W[i] = M[i] * k;
}

__kernel void vec_outer_prod_cl(__global rpca_kernel_datatype *V1,
									__global rpca_kernel_datatype *V2,
									__global rpca_kernel_datatype *V3,
									unsigned int m,
									unsigned int n)
{
	unsigned int i = get_global_id(0);
	unsigned int j = get_global_id(1);
	V3[(i * n) + j] = V1[i] * V2[j];
}

__kernel void mat_subt_cl(__global rpca_kernel_datatype *V1,
									__global rpca_kernel_datatype *V2,
									__global rpca_kernel_datatype *V3) {
	unsigned int i = get_global_id(0);
	V3[i] = V1[i] - V2[i];
}

__kernel void mat_transpose_cl1(__global rpca_kernel_datatype *a, __global rpca_kernel_datatype *b, int m, int n) {
	unsigned int i = get_global_id(0);
	unsigned int j = get_global_id(1);

	b[(j * m) + i] = a[(i * n) + j];
}

#define BLOCK_DIM 16
// This kernel is optimized to ensure all global reads and writes are coalesced,
// and to avoid bank conflicts in shared memory.  This kernel is up to 11x faster
// than the naive kernel below.  Note that the shared memory array is sized to
// (BLOCK_DIM+1)*BLOCK_DIM.  This pads each row of the 2D block in shared memory
// so that bank conflicts do not occur when threads address the array column-wise.
__kernel void mat_transpose_cl(__global rpca_kernel_datatype *idata,
								__global rpca_kernel_datatype *odata,
								unsigned int height,
								unsigned int width,
								__local rpca_kernel_datatype *block) {
	// read the matrix tile into shared memory
	unsigned int xIndex = get_global_id(0);
	unsigned int yIndex = get_global_id(1);

	if((xIndex < width) && (yIndex < height)) {
		unsigned int index_in = yIndex * width + xIndex;
		block[get_local_id(1)*(BLOCK_DIM+1)+get_local_id(0)] = idata[index_in];
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	// write the transposed matrix tile to global memory
	xIndex = get_group_id(1) * BLOCK_DIM + get_local_id(0);
	yIndex = get_group_id(0) * BLOCK_DIM + get_local_id(1);
	if((xIndex < height) && (yIndex < width)) {
		unsigned int index_out = yIndex * height + xIndex;
		odata[index_out] = block[get_local_id(0)*(BLOCK_DIM+1)+get_local_id(1)];
	}
}

__kernel void frob_norm_cl(__global rpca_kernel_datatype *g_idata, __global rpca_kernel_datatype *g_odata, unsigned int n, __local rpca_kernel_datatype* sdata) {
	// perform first level of reduction,
	// reading from global memory, writing to shared memory
	unsigned int tid = get_local_id(0);
	unsigned int i = get_group_id(0)*(get_local_size(0)*2) + get_local_id(0);

	sdata[tid] = (i < n) ? (g_idata[i] * g_idata[i]) : 0;
	if (i + get_local_size(0) < n) {
		sdata[tid] += (g_idata[i+get_local_size(0)] * g_idata[i+get_local_size(0)]);
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	// do reduction in shared mem
	for(unsigned int s=get_local_size(0)/2; s>0; s>>=1) {
		if (tid < s) {
			sdata[tid] += sdata[tid + s];
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}

	// write result for this block to global mem
	if (tid == 0) {
		g_odata[get_group_id(0)] = sdata[0];
	}
}

__kernel void vec_mat_copy_cl(__global rpca_kernel_datatype *a,
								__global rpca_kernel_datatype *b,
								unsigned int n,
								unsigned int j) {
	unsigned int i = get_global_id(0);
	b[(i * n) + j] = a[i];
}

__kernel void mat_mat_copy_cl(__global rpca_kernel_datatype *a,
								__global rpca_kernel_datatype *b) {
	unsigned int i = get_global_id(0);
	b[i] = a[i];
}

__kernel void mat_vec_spcl_op_cl(__global rpca_kernel_datatype *a,
									__global rpca_kernel_datatype *b,
									unsigned int n,
									unsigned int j) {
	unsigned int i = get_global_id(0);
	b[i] = a[(i * n) + j - 1] - a[(i * n) + j - 2];
}

__kernel void copy_single_val_cl(__global rpca_kernel_datatype *a,
									unsigned int n,
									rpca_kernel_datatype j) {
	a[n] = j;
}
