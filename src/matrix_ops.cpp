/*
* matrix_ops.cpp
*
*  Created on: Dec 11, 2014
*      Author: saikarthikreddyginni
*      Author: ankitgupta
*/

#include <cmath>
#include <cstdlib>
#include "matrix_ops.h"
#include "utils.h"

rpca_datatype frob_norm(rpca_datatype *a, cl_uint n) {
	rpca_datatype temp = 0;
	for (cl_uint i = 0; i < n; i++) {
		temp += (a[i] * a[i]);
	}

	return sqrt(temp);
}

void shrink_matrix(rpca_datatype *a, cl_uint n, rpca_datatype tau) {
	for (cl_uint i = 0; i < n; i++) {
		a[i] = shrinkage(a[i], tau);
	}
}

void add_matrix(rpca_datatype *a, rpca_datatype *b, rpca_datatype *c, cl_uint n) {
	for (cl_uint i = 0; i < n; i++) {
		c[i] = a[i] + b[i];
	}
}

void subtract_matrix(rpca_datatype *a, rpca_datatype *b, rpca_datatype *c, cl_uint n) {
	for (cl_uint i = 0; i < n; i++) {
		c[i] = a[i] - b[i];
	}
}

void scalar_mul_matrix(rpca_datatype *a, rpca_datatype *b, rpca_datatype k, cl_uint n) {
	for (cl_uint i = 0; i < n; i++) {
		b[i] = k * a[i];
	}
}

void matrix_transpose(rpca_datatype *a, cl_uint n) {
	for (cl_uint i = 0; i < n; i++) {
		for (cl_uint j = i; j < n; j++) {
			rpca_datatype temp = a[IDX(i, j, n)];
			a[IDX(i, j, n)] = a[IDX(j, i, n)];
			a[IDX(j, i, n)] = temp;
		}
	}
}

void matrix_multiplication(rpca_datatype *a, rpca_datatype *b, rpca_datatype *c, cl_uint m,
	cl_uint n, cl_uint p) {
	for (cl_uint i = 0; i < m; i++) {
		for (cl_uint j = 0; j < p; j++) {
			c[IDX(i, j, p)] = 0;
			for (cl_uint k = 0; k < n; k++) {
				c[IDX(i, j, p)] += (a[IDX(i, k, n)] * b[IDX(k, j, p)]);
			}
		}
	}
}

void matrix_transpose1(rpca_datatype *a, rpca_datatype *b, cl_uint m, cl_uint n) {
	for (cl_uint i = 0; i < m; i++) {
		for (cl_uint j = 0; j < n; j++) {
			b[IDX(j, i, m)] = a[IDX(i, j, n)];
		}
	}
}

void vector_summation_2nd(rpca_datatype *x, cl_uint n, rpca_datatype &s) {
	cl_uint i;
	s = 0.0;

	for (i = 0; i < n; i++) {
		s += x[i];
	}

	s = sqrt(s);
}

void exec_mat_mult_mnp_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_mem &c, cl_uint m, cl_uint n, cl_uint p) {
	cl_int clStatus;
	cl_uint tile_size = TILE_SIZE_MNP;
	size_t global_size[2] = { m, p };
	size_t local_size[2] = { tile_size, tile_size };

	/* Set the kernel arguments: */
	clStatus = clSetKernelArg(cl_params->kernel[0], 0, sizeof(cl_mem), (void *)&a);
	clStatus = clSetKernelArg(cl_params->kernel[0], 1, sizeof(cl_mem), (void *)&b);
	clStatus = clSetKernelArg(cl_params->kernel[0], 2, sizeof(cl_mem), (void *)&c);
	clStatus = clSetKernelArg(cl_params->kernel[0], 3, sizeof(unsigned int), &m);
	clStatus = clSetKernelArg(cl_params->kernel[0], 4, sizeof(unsigned int), &n);
	clStatus = clSetKernelArg(cl_params->kernel[0], 5, sizeof(unsigned int), &p);
	clStatus = clSetKernelArg(cl_params->kernel[0], 6, sizeof(unsigned int), &tile_size);
	clStatus = clSetKernelArg(cl_params->kernel[0], 7, tile_size * tile_size * sizeof(float), NULL);
	clStatus = clSetKernelArg(cl_params->kernel[0], 8, tile_size * tile_size * sizeof(float), NULL);

	/* Launch the GPU kernel*/
	clStatus = clEnqueueNDRangeKernel(cl_params->command_queue, cl_params->kernel[0], 2, NULL, global_size, local_size, 0, NULL, NULL);

	/* Wait for all commands in the queue to complete: */
	clStatus = clFinish(cl_params->command_queue);
}

void exec_mat_vec_mult_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_mem &c, cl_uint m, cl_uint n) {
	cl_int clStatus;
	size_t local_size = 1;
	size_t global_size;

	if (m == COL) {
		global_size = m;
	}
	else {
		global_size = m / MAT_VEC_GRP_SIZE;
	}

	/* Set the kernel arguments: */
	clStatus = clSetKernelArg(cl_params->kernel[1], 0, sizeof(cl_mem), (void *)&a);
	clStatus = clSetKernelArg(cl_params->kernel[1], 1, sizeof(cl_mem), (void *)&b);
	clStatus = clSetKernelArg(cl_params->kernel[1], 2, sizeof(cl_mem), (void *)&c);
	clStatus = clSetKernelArg(cl_params->kernel[1], 3, sizeof(unsigned int), &m);
	clStatus = clSetKernelArg(cl_params->kernel[1], 4, sizeof(unsigned int), &n);

	/* Launch the GPU kernel*/
	clStatus = clEnqueueNDRangeKernel(cl_params->command_queue, cl_params->kernel[1], 1, NULL, &global_size, &local_size, 0, NULL, NULL);

	/* Wait for all commands in the queue to complete: */
	clStatus = clFinish(cl_params->command_queue);
}

void exec_scalar_mat_mult_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_uint n, rpca_datatype scalar) {
	cl_int clStatus;
	const size_t global_size = n;
	const size_t local_size = 1;

	/* Set the kernel arguments: */
	clStatus = clSetKernelArg(cl_params->kernel[2], 0, sizeof(cl_mem), (void *)&a);
	clStatus = clSetKernelArg(cl_params->kernel[2], 1, sizeof(cl_mem), (void *)&b);
	clStatus = clSetKernelArg(cl_params->kernel[2], 2, sizeof(cl_float), &scalar);

	/* Launch the GPU kernel*/
	clStatus = clEnqueueNDRangeKernel(cl_params->command_queue, cl_params->kernel[2], 1, NULL, &global_size, NULL, 0, NULL, NULL);

	/* Wait for all commands in the queue to complete: */
	clStatus = clFinish(cl_params->command_queue);
}

void exec_vec_outer_prod_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_mem &c, cl_uint m, cl_uint n) {
	cl_int clStatus;
	size_t global_size[2] = { m, n };
	size_t local_size[2] = { 1, 1 };

	/* Set the kernel arguments: */
	clStatus = clSetKernelArg(cl_params->kernel[3], 0, sizeof(cl_mem), (void *)&a);
	clStatus = clSetKernelArg(cl_params->kernel[3], 1, sizeof(cl_mem), (void *)&b);
	clStatus = clSetKernelArg(cl_params->kernel[3], 2, sizeof(cl_mem), (void *)&c);
	clStatus = clSetKernelArg(cl_params->kernel[3], 3, sizeof(unsigned int), &m);
	clStatus = clSetKernelArg(cl_params->kernel[3], 4, sizeof(unsigned int), &n);

	/* Launch the GPU kernel*/
	clStatus = clEnqueueNDRangeKernel(cl_params->command_queue, cl_params->kernel[3], 2, NULL, global_size, NULL, 0, NULL, NULL);

	/* Wait for all commands in the queue to complete: */
	clStatus = clFinish(cl_params->command_queue);
}

void exec_mat_subt_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_mem &c, cl_uint n) {
	cl_int clStatus;
	const size_t global_size = n;
	const size_t local_size = 1;

	/* Set the kernel arguments: */
	clStatus = clSetKernelArg(cl_params->kernel[4], 0, sizeof(cl_mem), (void *)&a);
	clStatus = clSetKernelArg(cl_params->kernel[4], 1, sizeof(cl_mem), (void *)&b);
	clStatus = clSetKernelArg(cl_params->kernel[4], 2, sizeof(cl_mem), (void *)&c);

	/* Launch the GPU kernel*/
	clStatus = clEnqueueNDRangeKernel(cl_params->command_queue, cl_params->kernel[4], 1, NULL, &global_size, NULL, 0, NULL, NULL);

	/* Wait for all commands in the queue to complete: */
	clStatus = clFinish(cl_params->command_queue);
}

void exec_mat_transpose_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_uint m, cl_uint n) {
	cl_int clStatus;
	size_t local_size[2] = { BLOCK_DIM, BLOCK_DIM };
	size_t global_size[2] = { n, m };

	/* Set the kernel arguments: */

	clStatus = clSetKernelArg(cl_params->kernel[5], 0, sizeof(cl_mem), (void *)&a);
	clStatus = clSetKernelArg(cl_params->kernel[5], 1, sizeof(cl_mem), (void *)&b);
	clStatus = clSetKernelArg(cl_params->kernel[5], 2, sizeof(cl_uint), &m);
	clStatus = clSetKernelArg(cl_params->kernel[5], 3, sizeof(cl_uint), &n);
	clStatus = clSetKernelArg(cl_params->kernel[5], 4, (((BLOCK_DIM + 1) * BLOCK_DIM) * sizeof(rpca_datatype)), NULL);

	/* Launch the GPU kernel*/
	clStatus = clEnqueueNDRangeKernel(cl_params->command_queue, cl_params->kernel[5], 2, NULL, global_size, local_size, 0, NULL, NULL);

	/* Wait for all commands in the queue to complete: */
	clStatus = clFlush(cl_params->command_queue);
	clStatus = clFinish(cl_params->command_queue);
}

void exec_frob_norm_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_uint n, rpca_datatype &result) {
	cl_int clStatus;
	rpca_datatype *a_data = (rpca_datatype *)malloc(sizeof(rpca_datatype) * n);

	/* Read array data*/
	clStatus = clEnqueueReadBuffer(cl_params->command_queue, a, CL_TRUE, 0, n * sizeof(cl_float), (void *)a_data, 0, NULL, NULL);
	clStatus = clFinish(cl_params->command_queue);

	result = frob_norm(a_data, n);

	free(a_data);
}

void exec_vec_mat_copy_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_uint m, cl_uint n, cl_uint col) {
	cl_int clStatus;
	const size_t global_size = m;
	const size_t local_size = 1;

	/* Set the kernel arguments: */
	clStatus = clSetKernelArg(cl_params->kernel[7], 0, sizeof(cl_mem), (void *)&a);
	clStatus = clSetKernelArg(cl_params->kernel[7], 1, sizeof(cl_mem), (void *)&b);
	clStatus = clSetKernelArg(cl_params->kernel[7], 2, sizeof(unsigned int), &n);
	clStatus = clSetKernelArg(cl_params->kernel[7], 3, sizeof(unsigned int), &col);

	/* Launch the GPU kernel*/
	clStatus = clEnqueueNDRangeKernel(cl_params->command_queue, cl_params->kernel[7], 1, NULL, &global_size, NULL, 0, NULL, NULL);

	/* Wait for all commands in the queue to complete: */
	clStatus = clFinish(cl_params->command_queue);
}

void exec_mat_mat_copy_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_uint n) {
	cl_int clStatus;
	const size_t global_size = n;
	const size_t local_size = 1;

	/* Set the kernel arguments: */
	clStatus = clSetKernelArg(cl_params->kernel[8], 0, sizeof(cl_mem), (void *)&a);
	clStatus = clSetKernelArg(cl_params->kernel[8], 1, sizeof(cl_mem), (void *)&b);

	/* Launch the GPU kernel*/
	clStatus = clEnqueueNDRangeKernel(cl_params->command_queue, cl_params->kernel[8], 1, NULL, &global_size, &local_size, 0, NULL, NULL);

	/* Wait for all commands in the queue to complete: */
	clStatus = clFinish(cl_params->command_queue);
}

void exec_mat_vec_spcl_op_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_mem &b, cl_uint m, cl_uint n, cl_uint col) {
	cl_int clStatus;
	const size_t global_size = n;
	const size_t local_size = 1;

	/* Set the kernel arguments: */
	clStatus = clSetKernelArg(cl_params->kernel[9], 0, sizeof(cl_mem), (void *)&a);
	clStatus = clSetKernelArg(cl_params->kernel[9], 1, sizeof(cl_mem), (void *)&b);
	clStatus = clSetKernelArg(cl_params->kernel[9], 2, sizeof(unsigned int), &n);
	clStatus = clSetKernelArg(cl_params->kernel[9], 3, sizeof(unsigned int), &col);

	/* Launch the GPU kernel*/
	clStatus = clEnqueueNDRangeKernel(cl_params->command_queue, cl_params->kernel[9], 1, NULL, &global_size, &local_size, 0, NULL, NULL);

	/* Wait for all commands in the queue to complete: */
	clStatus = clFinish(cl_params->command_queue);
}

void exec_copy_single_val_kernel(rpca_cl_params *cl_params, cl_mem &a, cl_uint loc, rpca_datatype val) {
	cl_int clStatus;
	const size_t global_size = 1;
	const size_t local_size = 1;

	/* Set the kernel arguments: */
	clStatus = clSetKernelArg(cl_params->kernel[10], 0, sizeof(cl_mem), (void *)&a);
	clStatus = clSetKernelArg(cl_params->kernel[10], 1, sizeof(unsigned int), &loc);
	clStatus = clSetKernelArg(cl_params->kernel[10], 2, sizeof(rpca_datatype), &val);

	/* Launch the GPU kernel*/
	clStatus = clEnqueueNDRangeKernel(cl_params->command_queue, cl_params->kernel[10], 1, NULL, &global_size, &local_size, 0, NULL, NULL);

	/* Wait for all commands in the queue to complete: */
	clStatus = clFinish(cl_params->command_queue);
}