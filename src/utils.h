#ifndef UTILS_H_
#define UTILS_H_
#include "macros.h"

typedef struct rpca_cl_params_t {
	cl_platform_id *platforms;
	cl_uint num_platforms;
	cl_device_id *device_list;
	cl_uint num_devices;
	cl_context context;
	cl_command_queue command_queue;
	cl_program program;
	cl_kernel kernel[NUM_OF_KERNELS];
	cl_char *kernel_source;
	size_t kernel_src_size;
	cl_mem V_matrix_buf;
	cl_mem VT_matrix_buf;
	cl_mem Sigma_matrix_buf;
	cl_mem temp_buf;
	cl_mem V_vec_buf;
	cl_mem V_vec1_buf;
	cl_mem U_vec_buf;
	cl_mem U_vec_k_buf;
	cl_mem AT_matrix_buf;
	cl_mem U_matrix_buf;
	cl_mem L_buf;
} rpca_cl_params;

void disp_matrix_lin(rpca_datatype *a, cl_uint m, cl_uint n);
void disp_vector_lin(rpca_datatype *a, cl_uint m);
void disp_matrix_diag_lin(rpca_datatype *a, cl_uint n);
void pop_matrix_lin(rpca_datatype *a, cl_uint m, cl_uint n, cl_uint p);
void init_mat_zero(rpca_datatype *a, cl_uint n);
void init_mat_rand(rpca_datatype *a, cl_uint n);
void get_filename(char filename[], const char srcpath[], cl_uint j);
rpca_datatype shrinkage(rpca_datatype x, rpca_datatype tau);
bool cmp_data(rpca_datatype *a, rpca_datatype*b, cl_uint n);
void init_gpu_res_for_rpca(rpca_cl_params *cl_params);
void deinit_gpu_res_for_rpca(rpca_cl_params *cl_params);

#endif