/*
 * Image.h
 *
 *  Created on: Dec 14, 2014
 *      Author: saikarthikreddyginni
 */

#ifndef IMAGE_H_
#define IMAGE_H_

#include <fstream> // for file I/O
#include "macros.h"

using namespace std;

void imread(char* fileName, unsigned char *imageHeaderData, unsigned char *imagedata, cl_uint height, cl_uint width);
void imwrite(char* fileName, unsigned char *imageHeaderData, unsigned char *imagedata, cl_uint height, cl_uint width);

#endif /* IMAGE_H_ */
