#ifndef SVD_H_
#define SVD_H_
#include "macros.h"
#include "utils.h"

void svdcmp_linear(rpca_datatype *aat_eig_vec, cl_uint m, cl_uint n, rpca_datatype *sigma, rpca_datatype *ata_eig_vec);
void svdcmp_pow(rpca_datatype *aat_eig_vec, cl_uint m, cl_uint n, rpca_datatype *sigma, rpca_datatype *ata_eig_vec, rpca_datatype mu);
void svd_thresholding(rpca_datatype * M, rpca_datatype *S, rpca_datatype *Y, rpca_datatype *L, rpca_datatype mu);
void svd_pow_thresholding_combined(rpca_datatype * M, rpca_datatype *S, rpca_datatype *Y, rpca_datatype *L, rpca_datatype mu);
void svd_pow_thresholding_opencl(rpca_cl_params *cl_params, rpca_datatype * M, rpca_datatype *S, rpca_datatype *Y, rpca_datatype *L, rpca_datatype mu);
void init_svd_pow_thresholding_opencl();
void deinit_svd_pow_thresholding_opencl();

#endif