/*
* utils.cpp
*
*  Created on: Dec 11, 2014
*      Author: saikarthikreddyginni
*      Author: ankitgupta
*/

#include <iostream>
#include <iomanip>
#include "utils.h"
#include <cmath>

using namespace std;

void disp_matrix_lin(rpca_datatype *a, cl_uint m, cl_uint n)
{
	cout << "\ndisplaying matrix linear\n";
	for (cl_uint i = 0; i < m; i++)
	{
		for (cl_uint j = 0; j < n; j++)
		{
			cout << setw(9) << setprecision(3) << a[IDX(i, j, n)];
		}
		cout << endl << endl;
	}
}

void disp_matrix_diag_lin(rpca_datatype *a, cl_uint n)
{
	cout << "\ndisplaying matrix linear\n";
	for (cl_uint i = 0; i < n; i++)
	{
		cout << setw(15) << setprecision(3) << a[IDX(i, i, n)];
	}
}

void disp_vector_lin(rpca_datatype *a, cl_uint m)
{
	cout << "\ndisplaying matrix\n";
	for (cl_uint i = 0; i < m; i++)
	{
		cout << setw(15) << setprecision(3) << a[i];
	}
	cout << endl << endl;
}

void pop_matrix_lin(rpca_datatype *a, cl_uint m, cl_uint n, cl_uint p)
{
	for (cl_uint i = 0; i < m; i++)
	{
		for (cl_uint j = 0; j < n; j++)
		{
			srand((cl_uint)time(NULL) + i + j * 99 * p + p * p + 1099);
			a[IDX(i, j, n)] = (rpca_datatype)rand() / (rpca_datatype)RAND_MAX;
		}
	}
}

void init_mat_zero(rpca_datatype *a, cl_uint n) {
	for (cl_uint i = 0; i < n; i++) {
		a[i] = 0;
	}
}

void init_mat_rand(rpca_datatype *a, cl_uint n) {
	for (cl_uint j = 0; j < n; j++) {
		srand((cl_uint)time(NULL) + j + 8897);
		a[j] = (rpca_datatype)rand();
	}
}

bool cmp_data(rpca_datatype *a, rpca_datatype *b, cl_uint n)
{
	for (cl_uint i = 0; i < n; i++)
	{
		//cout << setw(15) << setprecision(3) << a[i] << setw(15) << setprecision(3) << b[i] << endl;
		if (a[i] != b[i])
		{
			return false;
		}
	}

	return true;
}

rpca_datatype shrinkage(rpca_datatype x, rpca_datatype tau) {
	rpca_datatype temp = MAX_R(fabs(x) - tau, 0);

	if (x < 0) {
		temp = -temp;
	}
	else if (x == 0) {
		temp = 0;
	}

	return temp;
}

void pop_string_by_int(char *c, cl_uint j)
{
	if ((j / 100)) {
		c[0] = 48 + (j / 100);
		c[1] = 48 + ((j / 10) % 10);
		c[2] = 48 + ((j % 100) % 10);
		c[3] = '\0';
	}
	else if (((j / 100) == 0) && (j / 10)) {
		c[0] = 48 + (j / 10);
		c[1] = 48 + (j % 10);
		c[2] = '\0';
	}
	else {
		c[0] = 48 + j;
		c[1] = '\0';
	}
}

void get_filename(char filename[], const char srcpath[], cl_uint j)
{
	char c[5];

	filename[0] = '\0';
	pop_string_by_int(c, j);

	strcat(filename, srcpath);
	strcat(filename, c);
	strcat(filename, FILE_TYPE);
}

int ReadSourceFromFile(const char* fileName, cl_char** source, size_t* sourceSize)
{
	int errorCode = CL_SUCCESS;

	FILE* fp = NULL;
#if _WIN32
	fopen_s(&fp, fileName, "rb");
#else
	fp = fopen(fileName, "rb");
#endif

	if (fp == NULL)
	{
		printf("Error: Couldn't find program source file '%s'.\n", fileName);
		errorCode = CL_INVALID_VALUE;
	}
	else
	{
		fseek(fp, 0, SEEK_END);
		*sourceSize = ftell(fp);
		fseek(fp, 0, SEEK_SET);

		*source = new cl_char[*sourceSize];
		if (*source == NULL)
		{
			printf("Error: Couldn't allocate %lu bytes for program source from file '%s'.\n", *sourceSize, fileName);
			errorCode = CL_OUT_OF_HOST_MEMORY;
		}
		else {
			fread(*source, 1, *sourceSize, fp);
		}
	}
	return errorCode;
}

void init_gpu_res_for_rpca(rpca_cl_params_t *cl_params) {
	cl_int clStatus;
	cl_uint i;
	cl_uint id = 0;
	cl_char queryBuffer[1024];

	cl_params->device_list = NULL;
	cl_params->num_platforms = 0;
	cl_params->kernel_src_size = 0;
	cl_params->kernel_source = NULL;

	clStatus = clGetPlatformIDs(0, NULL, &(cl_params->num_platforms));
	if (CL_SUCCESS != clStatus)
	{
		cout << "Error: clGetPlatformIDs " << clStatus << endl;
	}

	cl_params->platforms = (cl_platform_id *)malloc(sizeof(cl_platform_id) * cl_params->num_platforms);
	clStatus = clGetPlatformIDs(cl_params->num_platforms, cl_params->platforms, NULL);
	if (CL_SUCCESS != clStatus)
	{
		cout << "Error: clGetPlatformIDs " << clStatus << endl;
	}

	/* Find the Intel platform: */
	for (i = 0; i < cl_params->num_platforms; i++)
	{
		clStatus = clGetPlatformInfo(cl_params->platforms[i], CL_PLATFORM_NAME, 1024, &queryBuffer, NULL);
		if (strstr((const char *)queryBuffer, PLATFORM_NAME) != 0)
		{
			id = i;
		}
	}

	clStatus = clGetDeviceIDs(cl_params->platforms[id], CL_DEVICE_TYPE_GPU, 0, NULL, &(cl_params->num_devices));
	if (CL_SUCCESS != clStatus)
	{
		cout << "Error: clGetDeviceIDs " << clStatus << endl;
	}

	cl_params->device_list = (cl_device_id *)malloc(sizeof(cl_device_id) * cl_params->num_devices);
	clStatus = clGetDeviceIDs(cl_params->platforms[id], CL_DEVICE_TYPE_GPU, cl_params->num_devices, cl_params->device_list, NULL);
	if (CL_SUCCESS != clStatus)
	{
		cout << "Error: clGetDeviceIDs " << clStatus << endl;
	}

	/* Create context: */
	cl_context_properties contextProperties[] = { CL_CONTEXT_PLATFORM, (cl_context_properties)cl_params->platforms[id], 0 };
	cl_params->context = clCreateContextFromType(contextProperties, CL_DEVICE_TYPE_GPU, NULL, NULL, &clStatus);
	if (CL_SUCCESS != clStatus)
	{
		cout << "Error: clCreateContextFromType " << clStatus << endl;
	}

	/* Create command queue: */
	//cl_command_queue_properties properties = CL_QUEUE_PROFILING_ENABLE;

	char buf[128];
	clGetDeviceInfo(cl_params->device_list[0], CL_DEVICE_NAME, 128, buf, NULL);
	cout << "Running on Device: " << buf << endl << endl;

	cl_params->command_queue = clCreateCommandQueue(cl_params->context, cl_params->device_list[0], CL_QUEUE_PROFILING_ENABLE, &clStatus);
	if (CL_SUCCESS != clStatus)
	{
		cout << "Error: clCreateCommandQueue " << clStatus << endl;
	}

	clStatus = ReadSourceFromFile(SVD_KERNEL_FILE_PATH, &(cl_params->kernel_source), &(cl_params->kernel_src_size));
	if (CL_SUCCESS != clStatus)
	{
		cout << "Error: ReadSourceFromFile \n\n";
	}

	/* Create program: */
	cl_params->program = clCreateProgramWithSource(cl_params->context, 1, (const char **)&(cl_params->kernel_source), &(cl_params->kernel_src_size), &clStatus);
	if (CL_SUCCESS != clStatus)
	{
		cout << "Error: clCreateProgramWithSource " << clStatus << endl;
	}

#if (_WIN32 || __APPLE__)
	clStatus = clBuildProgram(cl_params->program, 1, cl_params->device_list, " -DUSE_FLOAT", NULL, NULL);
#else
	clStatus = clBuildProgram(cl_params->program, 1, cl_params->device_list, "", NULL, NULL);
#endif

	if (clStatus == CL_BUILD_PROGRAM_FAILURE)
	{
		// Determine the size of the log
		size_t log_size;
		clGetProgramBuildInfo(cl_params->program, cl_params->device_list[0], CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

		// Allocate memory for the log
		char *log = (char *)malloc(log_size);

		// Get the log
		clGetProgramBuildInfo(cl_params->program, cl_params->device_list[0], CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

		// Print the log
		printf("%s\n", log);
	}

	/* Create GPU kernels*/
	cl_params->kernel[0] = clCreateKernel(cl_params->program, "mat_mult_mnp_cl", &clStatus); // atrix Multiplication Kernel
	cl_params->kernel[1] = clCreateKernel(cl_params->program, "mat_vec_mult_cl", &clStatus); // Matrix Vector Multiplication Kernel
	cl_params->kernel[2] = clCreateKernel(cl_params->program, "scalar_mat_mult_cl", &clStatus); // Scalar Matrix Kernel
	cl_params->kernel[3] = clCreateKernel(cl_params->program, "vec_outer_prod_cl", &clStatus); // Vector Outer Product Kernel
	cl_params->kernel[4] = clCreateKernel(cl_params->program, "mat_subt_cl", &clStatus); // Matrix Subtraction Kernel
	cl_params->kernel[5] = clCreateKernel(cl_params->program, "mat_transpose_cl", &clStatus); // Matrix Transpose Kernel
	cl_params->kernel[6] = clCreateKernel(cl_params->program, "frob_norm_cl", &clStatus);// Frobenius Norm Kernel
	cl_params->kernel[7] = clCreateKernel(cl_params->program, "vec_mat_copy_cl", &clStatus); // Copy Vector to Matrix Kernel
	cl_params->kernel[8] = clCreateKernel(cl_params->program, "mat_mat_copy_cl", &clStatus); // Copy Matrix to Matrix Kernel
	cl_params->kernel[9] = clCreateKernel(cl_params->program, "mat_vec_spcl_op_cl", &clStatus); // Copy Subtraction of Two Matrix columns to Vector Kernel
	cl_params->kernel[10] = clCreateKernel(cl_params->program, "copy_single_val_cl", &clStatus); // Copy a Single Value to an Element of Matrix Kernel

	if (CL_SUCCESS != clStatus)
	{
		cout << "Error: clCreateKernel " << clStatus << endl;
	}

	/* Create GPU Buffers*/
	cl_params->V_matrix_buf = clCreateBuffer(cl_params->context, CL_MEM_READ_WRITE, COL * COL * sizeof(rpca_datatype), NULL, &clStatus);
	cl_params->VT_matrix_buf = clCreateBuffer(cl_params->context, CL_MEM_READ_WRITE, COL * COL * sizeof(rpca_datatype), NULL, &clStatus);
	cl_params->Sigma_matrix_buf = clCreateBuffer(cl_params->context, CL_MEM_READ_WRITE, COL * COL * sizeof(rpca_datatype), NULL, &clStatus);
	cl_params->temp_buf = clCreateBuffer(cl_params->context, CL_MEM_READ_WRITE, ROW * COL * sizeof(rpca_datatype), NULL, &clStatus);
	cl_params->V_vec_buf = clCreateBuffer(cl_params->context, CL_MEM_READ_WRITE, COL * sizeof(rpca_datatype), NULL, &clStatus);
	cl_params->V_vec1_buf = clCreateBuffer(cl_params->context, CL_MEM_READ_WRITE, COL * sizeof(rpca_datatype), NULL, &clStatus);
	cl_params->U_vec_buf = clCreateBuffer(cl_params->context, CL_MEM_READ_WRITE, ROW * sizeof(rpca_datatype), NULL, &clStatus);
	cl_params->U_vec_k_buf = clCreateBuffer(cl_params->context, CL_MEM_READ_WRITE, ROW * sizeof(rpca_datatype), NULL, &clStatus);
	cl_params->AT_matrix_buf = clCreateBuffer(cl_params->context, CL_MEM_READ_WRITE, ROW * COL * sizeof(rpca_datatype), NULL, &clStatus);
	cl_params->U_matrix_buf = clCreateBuffer(cl_params->context, CL_MEM_READ_WRITE, ROW * COL * sizeof(rpca_datatype), NULL, &clStatus);
	cl_params->L_buf = clCreateBuffer(cl_params->context, CL_MEM_READ_WRITE, ROW * COL * sizeof(rpca_datatype), NULL, &clStatus);

	if (CL_SUCCESS != clStatus)
	{
		cout << "Error: clCreateBuffer " << clStatus << endl;
	}
}

void deinit_gpu_res_for_rpca(rpca_cl_params_t *cl_params) {
	cl_int clStatus;

	clStatus = clReleaseMemObject(cl_params->V_matrix_buf);
	clStatus = clReleaseMemObject(cl_params->VT_matrix_buf);
	clStatus = clReleaseMemObject(cl_params->Sigma_matrix_buf);
	clStatus = clReleaseMemObject(cl_params->temp_buf);
	clStatus = clReleaseMemObject(cl_params->V_vec_buf);
	clStatus = clReleaseMemObject(cl_params->V_vec1_buf);
	clStatus = clReleaseMemObject(cl_params->U_vec_buf);
	clStatus = clReleaseMemObject(cl_params->U_vec_k_buf);
	clStatus = clReleaseMemObject(cl_params->AT_matrix_buf);
	clStatus = clReleaseMemObject(cl_params->U_matrix_buf);
	clStatus = clReleaseMemObject(cl_params->L_buf);

	clStatus = clReleaseKernel(cl_params->kernel[0]);
	clStatus = clReleaseKernel(cl_params->kernel[1]);
	clStatus = clReleaseKernel(cl_params->kernel[2]);
	clStatus = clReleaseKernel(cl_params->kernel[3]);
	clStatus = clReleaseKernel(cl_params->kernel[4]);
	clStatus = clReleaseKernel(cl_params->kernel[5]);
	clStatus = clReleaseKernel(cl_params->kernel[6]);
	clStatus = clReleaseKernel(cl_params->kernel[7]);
	clStatus = clReleaseKernel(cl_params->kernel[8]);
	clStatus = clReleaseKernel(cl_params->kernel[9]);
	clStatus = clReleaseKernel(cl_params->kernel[10]);

	clStatus = clReleaseProgram(cl_params->program);
	clStatus = clReleaseCommandQueue(cl_params->command_queue);
	clStatus = clReleaseContext(cl_params->context);
	free(cl_params->device_list);
	free(cl_params->platforms);
}
