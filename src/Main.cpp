/*
 * main.cpp
 *
 *  Created on: Dec 11, 2014
 *      Author: saikarthikreddyginni
 *      Author: ankitgupta
 */

#include <cstring>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include "svd.h"
#include "image.h"
#include "utils.h"
#include "matrix_ops.h"
#include "macros.h"
#include <cmath>

using namespace std;

int main(int argc, char** argv) {
	unsigned char *Ldata;
	unsigned char *Sdata;
	unsigned char *imageData;
	unsigned char *imageHeaderData;

	rpca_datatype *M;
	rpca_datatype *L;
	rpca_datatype *S;
	rpca_datatype *Y;
	rpca_datatype *Ys;
	rpca_datatype *T;
	rpca_cl_params *cl_params;

	rpca_datatype M_norm;
	rpca_datatype M_frob;
	rpca_datatype mu;
	rpca_datatype lambda;
	rpca_datatype delta;
	rpca_datatype frobNorm;
	rpca_datatype deltaMFrob;

	cl_uint iter;
	time_t timeStart;
	time_t timeEnd;

	M = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW * COL);
	L = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW * COL);
	S = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW * COL);
	Y = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW * COL);
	Ys = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW * COL);
	T = (rpca_datatype *)malloc(sizeof(rpca_datatype) * ROW * COL);

	imageData = (unsigned char *)malloc(sizeof(unsigned char) * IMG_ROW * IMG_COL);
	imageHeaderData = (unsigned char *)malloc(sizeof(unsigned char) * HEADER_LENGTH);
	Ldata = (unsigned char *)malloc(sizeof(unsigned char) * ROW);
	Sdata = (unsigned char *)malloc(sizeof(unsigned char) * ROW);

	cl_params = (rpca_cl_params *)malloc(sizeof(rpca_cl_params));
	init_gpu_res_for_rpca(cl_params);

	/* Load all video frames into feature matrix.*/
	iter = 0;
	for (cl_uint j = START_IMG_NO; j < END_IMG_NO; j = j + STEP) {
		char src_filename[300];
		get_filename(src_filename, SRC_PATH, j);
		cout << "Reading File : " << src_filename << endl << endl;

		imread(src_filename, imageHeaderData, imageData, IMG_ROW, IMG_COL);
		for (cl_uint p = 0; p < ROW; p++) {
			M[IDX(p, iter, COL)] = (rpca_datatype)(imageData[p]);
		}

		iter++;
	}

	M_norm = 0;
	M_frob = frob_norm(M, ROW * COL);

	for (cl_uint i = 0; i < COL; i++) {
		rpca_datatype temp = 0;
		for (cl_uint j = 0; j < ROW; j++) {
			temp += fabs(M[IDX(j, i, COL)]);
		}

		M_norm = MAX_R(M_norm, temp);
	}

	mu = (ROW * COL) / (4 * M_norm);
	lambda = 1 / rpca_datatype(sqrt(MAX_R(ROW, COL)));
	delta = rpca_datatype(pow(10, -7));

	init_mat_zero(L, ROW * COL);
	init_mat_zero(S, ROW * COL);
	init_mat_zero(Y, ROW * COL);
	init_mat_zero(T, ROW * COL);
	init_svd_pow_thresholding_opencl();

	subtract_matrix(M, L, T, ROW * COL);
	subtract_matrix(T, S, T, ROW * COL);

	frobNorm = frob_norm(T, ROW * COL);
	deltaMFrob = (delta * M_frob);

	iter = 0;
	cout << endl;
	while (frobNorm > deltaMFrob && iter < MAX_ITERATIONS) {
		iter++;
		//cout << "\nProcessing Iteration : " << iter << ", M-L-S : " << frobNorm << ", Delta * Frob(M) : " << deltaMFrob;
		time(&timeStart);

#if defined USE_SVD_OPENCL
		svd_pow_thresholding_opencl(cl_params, M, S, Y, L, mu);
#else
		svd_pow_thresholding_combined(M, S, Y, L, mu);
#endif

		subtract_matrix(M, L, S, ROW * COL);
		scalar_mul_matrix(Y, Ys, 1 / mu, ROW * COL);
		add_matrix(S, Ys, S, ROW * COL);
		shrink_matrix(S, ROW * COL, lambda / mu);

		subtract_matrix(M, L, T, ROW * COL);
		subtract_matrix(T, S, T, ROW * COL);
		scalar_mul_matrix(T, T, mu, ROW * COL);
		add_matrix(Y, T, Y, ROW * COL);

		subtract_matrix(M, L, T, ROW * COL);
		subtract_matrix(T, S, T, ROW * COL);
		frobNorm = frob_norm(T, ROW * COL);
		time(&timeEnd);

		cout << "Time For Iteration : " << iter << " is : " << timeEnd - timeStart << " Seconds" << endl;
	}

	iter = 0;
	for (cl_uint j = START_IMG_NO; j < END_IMG_NO; j = j + STEP)
	{
		char lowrank_filename[300];
		char sparse_filename[300];

		get_filename(lowrank_filename, LOWRANK_PATH, j);
		get_filename(sparse_filename, SPARSE_PATH, j);

		cout << "Writing File : " << lowrank_filename << endl << endl;
		cout << "Writing File : " << sparse_filename << endl;

		for (cl_uint i = 0; i < ROW; i++) {
			if (L[IDX(i, iter, COL)] > 255) {
				L[IDX(i, iter, COL)] = 255;
			}
			else if (L[IDX(i, iter, COL)] < 0) {
				L[IDX(i, iter, COL)] = 0;
			}

			if (S[IDX(i, iter, COL)] > 255) {
				S[IDX(i, iter, COL)] = 255;
			}
			else if (S[IDX(i, iter, COL)] < 0) {
				S[IDX(i, iter, COL)] = 0;
			}

			Ldata[i] = (unsigned char)L[IDX(i, iter, COL)];
			Sdata[i] = (unsigned char)S[IDX(i, iter, COL)];
		}

		imwrite(lowrank_filename, imageHeaderData, Ldata, IMG_ROW, IMG_COL);
		imwrite(sparse_filename, imageHeaderData, Sdata, IMG_ROW, IMG_COL);
		iter++;
	}

	deinit_gpu_res_for_rpca(cl_params);
	deinit_svd_pow_thresholding_opencl();

	free(Ldata);
	free(Sdata);
	free(cl_params);
	free(L);
	free(S);
	free(Y);
	free(Ys);
	free(T);
	free(imageData);
	free(imageHeaderData);
	system("pause");
	return 0;
}

